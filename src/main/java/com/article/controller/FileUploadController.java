package com.article.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
public class FileUploadController {
    @GetMapping("/upload")
    public String upload(){
        return "upload";
    }

    @PostMapping("/upload")
    public String saveFile(@RequestParam("file")MultipartFile file){
        System.out.println(file.getOriginalFilename());
        String serverPath="C:\\Spring-Image/";
        String fileName= UUID.randomUUID().toString()+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
        if(!file.isEmpty()){
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath,fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/upload";
    }
//    @GetMapping("/upload")
//    public String upload(){
//        return "upload";
//    }
//
//    @PostMapping("/upload")
//    public String saveFile(@RequestParam("file")MultipartFile file){
//        System.out.println(file.getOriginalFilename());
//        String serverPath="C:\\Users\\RothNak\\Desktop\\HRD\\Spring\\Image/";
//        String fileName= UUID.randomUUID().toString()+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
//        if(!file.isEmpty()){
//            try {
//                Files.copy(file.getInputStream(), Paths.get(serverPath,fileName));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return "redirect:/upload";
//    }

}
