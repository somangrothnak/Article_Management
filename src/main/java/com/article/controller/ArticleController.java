package com.article.controller;

import com.article.model.Article;
import com.article.model.ArticleFilter;
import com.article.model.Category;
import com.article.service.article.ArticleService;
import com.article.service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class ArticleController {
    int i=1;

    private ArticleService articleService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    public ArticleController(ArticleService articleService){
        this.articleService=articleService;
    }

//    @GetMapping("/tables")
//    public String article(){
//        return "tables";
//    }
    @GetMapping("/add")
    public String addToForm(ArticleFilter filter, ModelMap model){
        model.addAttribute("id",i);
        model.addAttribute("categories",categoryService.findAll());
        model.addAttribute("article",new Article());
        model.addAttribute("filter",filter);
        model.addAttribute("categories",categoryService.findAll());
        return "add";
    }
    @PostMapping("/save")
    public String saveArticle(@RequestParam("file")MultipartFile file,@Valid @ModelAttribute Article article,ArticleFilter filter, BindingResult bindingResult,ModelMap model){
        System.out.println("Hello");
        if(bindingResult.hasErrors()==true){
            System.out.println(bindingResult.hasErrors());
            System.out.println(file.getOriginalFilename());
            model.addAttribute("id",i);
            model.addAttribute("categories",categoryService.findAll());
            model.addAttribute("filter",filter);
            return "add";
        }
        if(file.isEmpty()){
            return "add";
        }
        i=i+1;
        String serverPath="C:\\Spring-Image/";
        String fileName= UUID.randomUUID().toString()+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
        System.out.println(fileName);
        if(!file.isEmpty()){
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath,fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        model.addAttribute("article",article);
        model.addAttribute("categories",categoryService.findAll());
        model.addAttribute("filter",filter);
        model.addAttribute("categories",categoryService.findAll());
        article.setCategory(categoryService.findOne(article.getCategory().getId()));
        article.setCreatedDate(new Date().toString());
        article.setImage("/Image/"+fileName);

        System.out.println(article);
        articleService.add(article);
        return "redirect:/add";//return to GetMapping("/add")
    }

    @GetMapping("/tables")
    public String getAll(ArticleFilter filter, ModelMap model){
        System.out.println("Title:"+filter);
        List<Article> list=articleService.findAllFilter(filter);
        model.addAttribute("articles",list);
        model.addAttribute("filter",filter);
        model.addAttribute("categories",categoryService.findAll());
        for(int i=0;i<list.size();i++){
            System.out.println(list);
        }
        return "tables";
    }

    @GetMapping("/detete/{id}")
    public String delete(@PathVariable("id") int id){
        articleService.delete(id);
        return "redirect:/tables";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") int id,ArticleFilter filter,ModelMap model){
        model.addAttribute("article",articleService.findOne(id));
        model.addAttribute("filter",filter);
        model.addAttribute("categories",categoryService.findAll());
        return "view";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") int id,ArticleFilter filter,ModelMap model){
        model.addAttribute("article",articleService.findOne(id));
        model.addAttribute("filter",filter);
        model.addAttribute("categories",categoryService.findAll());
        return "update";
    }

    @PostMapping("/update")
    public String saveUpdate(@RequestParam("file")MultipartFile file,@ModelAttribute Article article, BindingResult bindingResult,ModelMap modelMap){
        if(file.isEmpty()){
            article.setImage(articleService.findOne(article.getId()).getImage());
        }
        String serverPath="C:\\Spring-Image/";
        String fileName= UUID.randomUUID().toString()+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
        System.out.println(fileName);
        if(!file.isEmpty()){
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath,fileName));
                article.setImage("/Image/"+fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        articleService.update(article);
        return "redirect:/tables";
    }
}
