package com.article.repository.article;

import com.article.model.Article;
import com.article.model.ArticleFilter;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
//@Repository
public class ArticleRepositoryImpl implements ArticleRepository {
    List<Article> articleList=new ArrayList<>();


    @Override
    public Boolean add(Article article) {
        articleList.add(article);

        return null;
    }

    @Override
    public List<Article> findAll() {
        return articleList;
    }

    @Override
    public Article findOne(int id) {
        for(Article list:articleList){
            if(list.getId()==id){
                return list;
            }
        }
        return null;
    }

    @Override
    public void delete(int id) {
        for(Article list:articleList){
            if(list.getId()==id){
                articleList.remove(list);
                return;
            }
        }
    }

    @Override
    public void update(Article article) {

    }

    @Override
    public List<Article> findAllFilter(ArticleFilter filter) {
        return null;
    }


}
