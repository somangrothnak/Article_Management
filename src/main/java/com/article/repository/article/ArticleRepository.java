package com.article.repository.article;

import com.article.model.Article;
import com.article.model.ArticleFilter;
import com.article.repository.provider.ArticleProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ArticleRepository {
    @Insert("INSERT INTO tb_articles(name,description,author,image,created_date,category_id) VALUES (#{name},#{description},#{author},#{image},#{createdDate},#{category.id})")
//    @Insert("INSERT INTO tb_articles(name,description,author,createdDate,image,category_id) VALUES(#{name},#{description},#{author},#{createdDate},#{image},#{category.id}")
    public Boolean add(Article article);

    @Select("select a.id,a.name,a.description,a.author,a.image,a.category_id,c.name as category_name,a.created_date FROM tb_articles a INNER JOIN tb_categories c ON a.category_id=c.id order by a.id asc")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "name",column = "name"),
            @Result(property = "description",column = "description"),
            @Result(property = "author",column = "author"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "image",column = "image"),
            @Result(property = "category.id",column = "id"),
            @Result(property = "category.name",column = "category_name")

    })
    public List<Article> findAll();

    @Select("select a.id,a.name,a.description,a.author,a.image,a.category_id,c.name as category_name,a.created_date FROM tb_articles a INNER JOIN tb_categories c ON a.category_id=c.id WHERE a.id=#{id}")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "name",column = "name"),
            @Result(property = "description",column = "description"),
            @Result(property = "author",column = "author"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "image",column = "image"),
            @Result(property = "category.id",column = "id"),
            @Result(property = "category.name",column = "category_name")
    })
    public Article findOne(int id);

    @Delete("delete from tb_articles where id=#{id}")
    public void delete(int id);

    @Update("update tb_articles set name=#{name},description=#{description},image=#{image},author=#{author},category_id=#{category.id} where id=#{id}")
    public void update(Article article);

    @SelectProvider(method = "findAllFilter",type = ArticleProvider.class)
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "name",column = "name"),
            @Result(property = "description",column = "description"),
            @Result(property = "author",column = "author"),
            @Result(property = "createdDate",column = "created_date"),
            @Result(property = "image",column = "image"),
            @Result(property = "category.id",column = "id"),
            @Result(property = "category.name",column = "category_name")
    })
    public List<Article> findAllFilter(ArticleFilter filter);
}
