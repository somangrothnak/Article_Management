package com.article.repository.provider;

import com.article.model.ArticleFilter;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {
    public String findAllFilter(ArticleFilter filter){
        return new SQL(){{
            SELECT("a.id,a.name,a.description,a.author,a.image,a.category_id,c.name as category_name,a.created_date");
            FROM("tb_articles a");
            INNER_JOIN("tb_categories c ON a.category_id=c.id");
            if(filter.getTitle()!=null){
                WHERE("a.name ILIKE '%' || #{title} || '%'");
            }
            if(filter.getCate_id()!=null){
                WHERE("a.category_id=#{cate_id}");
            }

            ORDER_BY("a.id ASC");
        }}.toString();
    }
}
