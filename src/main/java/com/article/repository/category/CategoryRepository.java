package com.article.repository.category;
import com.article.model.Category;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CategoryRepository {
    @Select("select id,name from tb_categories order by id asc")
    List<Category> findAll();
    @Select("select id,name from tb_categories where id=#{id}")
    Category findOne(int id);
}
