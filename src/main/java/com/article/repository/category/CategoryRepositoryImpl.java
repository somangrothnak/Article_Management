package com.article.repository.category;

import com.article.model.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
//@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private List<Category> categoryList=new ArrayList<>();

    public CategoryRepositoryImpl(){
        categoryList.add(new Category(1,"Java"));
        categoryList.add(new Category(2,"Spring"));
        categoryList.add(new Category(3,"Korea"));
        categoryList.add(new Category(4,"Japen"));
        categoryList.add(new Category(5,"Web"));
        categoryList.add(new Category(6,"Database"));

    }
    @Override
    public List<Category> findAll() {
        return categoryList;
    }

    @Override
    public Category findOne(int id) {
        for(Category category:categoryList){
            if (category.getId()==id){
                return category;
            }
        }
        return null;
    }
}
