package com.article.service.category;

import com.article.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> findAll();
    Category findOne(int id);
}
