package com.article.service.article;

import com.article.model.Article;
import com.article.model.ArticleFilter;

import java.util.List;

public interface ArticleService {
    public Boolean add(Article article);
    public List<Article> findAll();
    public Article findOne(int id);
    void delete(int id);
    void update(Article article);
    List<Article> findAllFilter(ArticleFilter filter);
}
