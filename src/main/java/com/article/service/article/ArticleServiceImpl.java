package com.article.service.article;

import com.article.model.Article;
import com.article.model.ArticleFilter;
import com.article.repository.article.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ArticleServiceImpl implements  ArticleService{

    private ArticleRepository articleRepository;
    @Autowired
    public ArticleServiceImpl(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public Boolean add(Article article) {
        articleRepository.add(article);
        return null;
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public Article findOne(int id) {
//        articleRepository.findOne(id);
        return articleRepository.findOne(id);
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }

    @Override
    public void update(Article article) {
        articleRepository.update(article);
    }

    @Override
    public List<Article> findAllFilter(ArticleFilter filter) {
        return articleRepository.findAllFilter(filter);
    }


}
