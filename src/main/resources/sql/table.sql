create table tb_categories(
 id int primary key auto_increment,
name varchar not null
);
create table tb_articles(
id int primary key auto_increment,
name varchar not null,
description text not null,
author varchar not null,
created_date varchar,
image varchar not null,
category_id int references tb_categories(id) on Delete cascade on update cascade
)